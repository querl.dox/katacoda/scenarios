Download and start Docker image.

Filesystem mounted on host at /root:/root.

Python 3 RC on Alpine

`docker run -d -it \
  --name python-alpine \
  --mount type=bind,source=/root,target=/root \
  python:rc-alpine \
  /bin/sh`{{execute}}

## OR

Python 3 RC on Debian Buster

`docker run -d -it \
  --name python-debian \
  --mount type=bind,source=/root,target=/root \
  python:rc-buster \
  /bin/bash`{{execute}}



Connect to docker container.

`docker attach python-alpine`{{execute}}

`docker attach python-debian`{{execute}}

List running docker containers.

`docker ps -a`{{execute}}

