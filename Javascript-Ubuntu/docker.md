Download and start Docker image.

Filesystem mounted on host at /root:/root.

live-server running on port 8085

Clone repo.

`git clone https://gitlab.com/querl.dox/clarence-daniels-jr/book/notes_to_the_future/notes/ /root/notes`{{execute}}

Start docker container

`docker run -d -it \
  --name javascript \
  --mount type=bind,source=/root,target=/tmp \
  -p 8085-8087:8085-8087/tcp \
  registry.gitlab.com/querl.dox/clarence-daniels-jr/book/notes_to_the_future/build:master \
  /bin/sh`{{execute}}

Connect to docker container.

`docker attach javascript`{{execute}}


List running docker containers.

`docker ps -a`{{execute}}


Remove docker containers.

`docker rm -f -v typescript`{{execute}}
