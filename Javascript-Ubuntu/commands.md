[MDN web docs](https://developer.mozilla.org/en-US/docs/Learn/JavaScript)


Copy project files

and 

Start live-server on port 8085

`cp -r /project /tmp/ && live-server --port=8085 --no-browser /tmp/project/dev &`{{execute}}


 Start terser.

`terser /tmp/project/dev/scripts/entity/panel.js /tmp/project/dev/scripts/entity/task.js \
/tmp/project/dev/scripts/app/init.js /tmp/project/dev/scripts/app/main.js --verbose \
--ecma 8 --output /tmp/project/dev/scripts/compiled.js`{{execute}}


Compile typescript and watch

`tsc --build /tmp/project/katacoda-tsconfig.json -w &`{{execute}}


Compile sass and watch

`node-sass --source-map true --output-style nested --output /tmp/project/dev/styles /tmp/project/src/stylesheets/main.scss`{{execute}}

