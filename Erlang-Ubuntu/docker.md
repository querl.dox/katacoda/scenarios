Download and start Docker image.

Filesystem mounted on host at /root:/root.


`docker run -d -it \
  --name erlang \
  --mount type=bind,source=/root,target=/root \
  erlang:alpine \
  /bin/sh`{{execute}}

## OR

`docker run -d -it \
  --name erlang \
  --mount type=bind,source=/root,target=/root \
  registry.gitlab.com/querl.dox/erlang/build:dev-image \
  /bin/sh`{{execute}}



Connect to docker container.

`docker attach erlang`{{execute}}


List running docker containers.

`docker ps -a`{{execute}}

