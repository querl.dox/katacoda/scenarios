#!/bin/bash

echo install.sh

docker run -d -it \
  --name rust \
  --mount type=bind,source=/root,target=/root \
  registry.gitlab.com/querl.dox/linux/alpine_linux/build:rust \
  /bin/sh

docker attach rust