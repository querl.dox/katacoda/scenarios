 <div class="section" id="using-ubuntu-for-your-development-environment">
<h1>Using Ubuntu for Your Development Environment<a class="headerlink" href="#using-ubuntu-for-your-development-environment" title="Permalink to this headline">¶</a></h1>
<p>This procedure explains how to set up Hyperledger Sawtooth for application
development on Ubuntu 16.04. It shows you how to install Sawtooth on Ubuntu,
then walks you through the following tasks:</p>
<blockquote>
<div><ul class="simple">
<li><p>Generating a user key</p></li>
<li><p>Creating the genesis block</p></li>
<li><p>Generating a root key</p></li>
<li><p>Starting the components: validator, consensus engine, REST API, and
transaction processors</p></li>
<li><p>Checking the status of the REST API</p></li>
<li><p>Using Sawtooth commands to submit transactions, display block data, and view
global state</p></li>
<li><p>Examining Sawtooth logs</p></li>
<li><p>Stopping Sawtooth and resetting the development environment</p></li>
</ul>
</div></blockquote>
<p>After completing this procedure, you will have the application development
environment that is required for the other tutorials in this guide. The next
tutorial introduces the XO transaction family by using the <code class="docutils literal notranslate"><span class="pre">xo</span></code> client
commands to play a game of tic-tac-toe. The final set of tutorials describe how
to use an SDK to create a transaction family that implements your application’s
business logic.</p>
<div class="section" id="about-the-application-development-environment">
<h2>About the Application Development Environment<a class="headerlink" href="#about-the-application-development-environment" title="Permalink to this headline">¶</a></h2>
<p>The Ubuntu application development environment is a single validator node that
is running a validator, a REST API, and three transaction processors. This
environment uses Developer mode (dev mode) consensus and serial transaction
processing.</p>
<div class="figure align-center">
<a class="reference internal image-reference" href="../_images/appdev-environment-one-node-3TPs.svg"><img alt="Ubuntu: Sawtooth application environment with one node" src="../_images/appdev-environment-one-node-3TPs.svg" width="100%" /></a>
</div>
<p>This environment introduces basic Sawtooth functionality with the
<a class="reference external" href="../transaction_family_specifications/integerkey_transaction_family">IntegerKey</a>
and
<a class="reference external" href="../transaction_family_specifications/settings_transaction_family">Settings</a>
transaction processors for the business logic and Sawtooth commands as a client.
It also includes the
<a class="reference external" href="../transaction_family_specifications/xo_transaction_family">XO</a>
transaction processor, which is used in later tutorials.</p>
<p>The IntegerKey and XO families are simple examples of a transaction family, but
Settings is a reference implementation. In a production environment, you should
always run a transaction processor that supports the Settings transaction
family.</p>
<p>In this procedure, you will open seven terminal windows on your host system: one
for each Sawtooth component and one to use for client commands.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>This procedure starts the validator first, then the REST API, followed by
the transaction processors. However, the start-up order is flexible. For
example, you can start the transaction processors before starting the
validator.</p>
</div>
</div>
<div class="section" id="prerequisites">
<h2>Prerequisites<a class="headerlink" href="#prerequisites" title="Permalink to this headline">¶</a></h2>
<p>This Sawtooth development environment requires Ubuntu 16.04.</p>
</div>