<div class="admonition note">
<p class="admonition-title">Note</p>
<p>This application development environment is used in later procedures in this
guide. Do not stop this environment if you intend to continue with these
procedures.</p>
</div>
<p>To stop the Sawtooth components:</p>
<ol class="arabic">
<li><p>Stop the validator by entering CTRL-c in the validator terminal window.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>A single CTRL-c does a graceful shutdown. If you prefer not to wait, you
can enter multiple CTRL-c characters to force the shutdown.</p>
</div>
</li>
<li><p>Stop the Devmode consensus engine by entering a single CTRL-c in consensus terminal window.</p></li>
<li><p>Stop the REST API by entering a single CTRL-c in REST API terminal window.</p></li>
<li><p>Stop each transaction processor by entering a single CTRL-c in the
appropriate window.</p></li>
</ol>
<p>You can restart the Sawtooth components at a later time and continue working
with your application development environment.</p>
<p>To completely reset the Sawtooth environment and start over from the beginning
of this procedure, add these steps:</p>
<ul class="simple">
<li><p>To delete the blockchain data, remove all files from <code class="docutils literal notranslate"><span class="pre">/var/lib/sawtooth</span></code>.</p></li>
<li><p>To delete the Sawtooth logs, remove all files from <code class="docutils literal notranslate"><span class="pre">/var/log/sawtooth/</span></code>.</p></li>
<li><p>To delete the Sawtooth keys, remove the key files