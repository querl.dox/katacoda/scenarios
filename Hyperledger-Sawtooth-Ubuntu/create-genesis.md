<p>Because this is a new network, you must create a genesis block (the first block
on the distributed ledger). This step is done only for the first validator node
on the network. Validator nodes that join an existing network do not create
a genesis block.</p>

<p>The genesis block contains initial values that are necessary when a Sawtooth
distributed ledger is created and used for the first time, including the keys
for users who are authorized to set and change configuration settings.</p>

<p>Create a settings proposal (as a batch of transactions) that authorizes you
to set and change configuration settings. By default (if no options are
specified), the <code class="docutils literal notranslate"><span class="pre">sawset</span> <span class="pre">genesis</span></code> command uses the key of the current user
(you).</p>

`sawset genesis && sawadm genesis config-genesis.batch`{{execute}}


