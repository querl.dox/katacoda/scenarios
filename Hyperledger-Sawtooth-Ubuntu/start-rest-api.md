The REST API allows you to configure a running validator, submit batches, and query the state of the distributed ledger.

`sawtooth-rest-api -v`{{execute}}

The rest-api terminal window continues display log messages.