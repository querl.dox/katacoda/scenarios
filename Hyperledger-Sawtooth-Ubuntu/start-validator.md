Start a validator that listens locally on the default ports. 
The validator terminal window displays verbose log messages.
Note that the validator is waiting for the Settings transaction processor (sawtooth_settings) to start.

`sawtooth-validator -vv`{{execute}}

The validator terminal window will continue to display log messages.
