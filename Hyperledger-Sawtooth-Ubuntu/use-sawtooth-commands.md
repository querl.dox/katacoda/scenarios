
### Creating and Submitting Transactions with intkey.
Use intkey create_batch to prepare batches of transactions that set a few keys to random values, then randomly increment and decrement those values. These batches are saved locally in the file batches.intkey.

`intkey create_batch --count 10 --key-count 5`{{execute}}

Use intkey load to submit the batches to the validator.

`intkey load -f batches.intkey`{{execute}}

You can also look at the Sawtooth log files to see what happened.

`bash -c "tail -10 /var/log/sawtooth/intkey-*-debug.log"`{{execute}}


### Submitting Transactions with sawtooth batch submit
Create a batch of transactions.

`intkey create_batch --count 10 --key-count 5`{{execute}}

Submit the batch file.

`sawtooth batch submit -f batches.intkey`{{execute}}


### Viewing Blockchain and Block Data with sawtooth block
Use sawtooth block list to display the list of blocks stored in state.

`sawtooth block list`{{execute}}

From the output generated by sawtooth block list, copy the ID of a block you want to view, then paste it in place of {BLOCK_ID} in the following command:

user@client$ sawtooth block show {BLOCK_ID}

### Viewing State Data with sawtooth state
Use sawtooth state list to list the nodes (addresses) in state.

`sawtooth state list`{{execute}}

Use sawtooth state show to view state data at a specific address (a node in the Merkle-Radix database). Copy the address from the output of sawtooth state list, then paste it in place of {STATE_ADDRESS} in the following command:

user@client$ sawtooth state show {STATE_ADDRESS}