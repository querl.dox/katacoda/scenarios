By default, Sawtooth logs are stored in the directory /var/log/sawtooth. Each component (validator, REST API, and transaction processors) has both a debug log and an error log.

`ls -1 /var/log/sawtooth`{{execute}}