Start the Devmode consensus engine that decides what block to add to a blockchain. 

`devmode-engine-rust -vv --connect tcp://localhost:5050`{{execute}}

The consensus terminal window displays verbose log messages showing the Devmode engine connecting to and registering with the validator. 