#!/bin/bash

echo "Hello World"

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8AA7AF1F1091A5FD
add-apt-repository 'deb [arch=amd64] http://repo.sawtooth.me/ubuntu/bumper/stable xenial universe'
apt-get update
apt-get install -y sawtooth
apt-get install sawtooth-devmode-engine-rust
dpkg -l '*sawtooth*'