Add stable repository. 

`apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8AA7AF1F1091A5FD \
&& add-apt-repository 'deb [arch=amd64] http://repo.sawtooth.me/ubuntu/bumper/stable xenial universe' \
&& apt-get update`{{execute}}

## OR

Add nightly repository. 

`apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 44FC67F19B2466EA \
&& add-apt-repository 'deb [arch=amd64] http://repo.sawtooth.me/ubuntu/bumper/nightly xenial universe' \
&& apt-get update`{{execute}}


Install Sawtooth packages. 

`apt-get install -y sawtooth`{{execute}}


Install the Sawtooth Devmode consensus engine package. 

` apt-get install sawtooth-devmode-engine-rust`{{execute}}


View Sawtooth Packages 

`dpkg -l '*sawtooth*'`{{execute}}
