## Open a new terminal window for each transaction processor. 

Start the Settings transaction processor

`settings-tp -v`{{execute}}

The settings-tp transaction processor continues to run and to display log messages in its terminal window.

Check the validator terminal window to confirm that the transaction processor has registered with the validator.


At this point, you can see the authorized keys setting that was proposed.

`sawtooth settings list`{{execute}}


Start the IntegerKey transaction processor.

`intkey-tp-python -v`{{execute}}

Check the validator terminal window to confirm that the transaction processor has registered with the validator. 


(Optional) Start the XO transaction processor.

`xo-tp-python -v`{{execute}}

Check the validator terminal window to confirm that the transaction processor has registered with the validator.