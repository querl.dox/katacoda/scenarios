Download and start Docker image.

Filesystem mounted on host at /root:/root.

live-server running on port 8085


`docker run -d -it \
  --name typescript \
  --mount type=bind,source=/root,target=/tmp \
  -p 8085-8087:8085-8087/tcp \
  registry.gitlab.com/querl.dox/js/typescript/build:v0001 \
  /bin/sh`{{execute}}

Connect to docker container.

`docker attach typescript`{{execute}}


List running docker containers.

`docker ps -a`{{execute}}


Remove docker containers.

`docker rm -f -v typescript`{{execute}}


