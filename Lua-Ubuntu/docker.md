Download and start Docker image.

Filesystem mounted on host at /root:/root.


`docker run -d -it \
  --name lua \
  --mount type=bind,source=/root,target=/root \
  registry.gitlab.com/querl.dox/linux/alpine_linux/build:lua \
  /bin/sh`{{execute}}



Connect to docker container.

`docker attach lua`{{execute}}


List running docker containers.

`docker ps -a`{{execute}}

