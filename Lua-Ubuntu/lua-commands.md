[Lua 5.3 Reference Manual](https://www.lua.org/manual/5.3/)

[The.Lua.Tutorial](http://luatut.com/)

[lua-users Tutorial Directory](http://lua-users.org/wiki/TutorialDirectory)

[Programming in Lua (first edition)](https://www.lua.org/pil/contents.html)

[tutorialspoint Lua Tutorial](https://www.tutorialspoint.com/lua/index.htm)


// Start erlang shell.

// `erl`{{execute}}


// Clear all variables.

// `f(). `{{execute}}

