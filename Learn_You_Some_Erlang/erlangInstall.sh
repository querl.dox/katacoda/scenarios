#!/bin/bash

echo "Hello World"

docker run -d \
  -it \
  --name erlangdev-container \
  --mount type=bind,source=/root,target=/root \
  erlang:alpine /bin/sh
