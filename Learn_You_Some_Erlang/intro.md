## Connect to docker container
<pre>
docker attach erlangdev-container
</pre>

## List running docker containers
<pre>
docker ps -a
</pre>

Filesystem mounted on host at /root:/root